const request = require('request')

const darkSky_secretKey = process.env.DARKSKY_KEY;

const forecast = (latitude, longitude, callback) => {

    const darkSky_url = `https://api.darksky.net/forecast/${darkSky_secretKey}/${latitude},${longitude}?units=si`

    request({ url: darkSky_url, json: true }, (error, response) => {
        if (error) {
            callback('Unable to connect to weather service!', undefined)
        } else if (response.body.error) {
            callback('Unable to find location!', undefined)
        } else {
            callback(undefined, response.body.currently)
        }
    }) 
}

module.exports = forecast
  